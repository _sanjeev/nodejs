const fs = require("fs");

fs.mkdir("hello", (err, data) => {
    if (err) {
        console.log("Error", err);
        return;
    }
    console.log("Directory Created");
});

fs.writeFile("./hello/index.txt", "I am from India.", (err, data) => {
    if (err) {
        console.log("Error", err);
        return;
    }
    console.log("File Created Successfully");
});

fs.appendFile("./hello/index.txt", "Love", (err) => {
    if (err) {
        console.log(err);
        return;
    }
    console.log("Append Successfully");
});

fs.rename("./hello/index.txt", "./hello/a.txt", (err) => {
    if (err) {
        console.log("Error", err);
        return;
    }
    console.log("File rename successfully");
});

fs.readFile("./hello/a.txt", "utf-8", (err, data) => {
    if (err) {
        console.log("Error", err);
        return;
    }
    console.log(data);
});

fs.unlink("./hello/a.txt", (err) => {
    if (err) {
        console.log("Error", err);
        return;
    }
    console.log("Delete file successfully");
});

fs.rmdir("./hello", (err) => {
    if (err) {
        console.log("Error", err);
        return;
    }
    console.log("Successfully delete the directory");
});

fs.rename("./hello", "./hai", (err) => {
    if (err) {
        console.log("Error", err);
        return;
    }
    console.log("Successfully rename the file");
});
